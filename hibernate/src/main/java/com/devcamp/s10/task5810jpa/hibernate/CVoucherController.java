package com.devcamp.s10.task5810jpa.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.task5810jpa.hibernate.model.CVoucher;
import com.devcamp.s10.task5810jpa.hibernate.repository.IVoucherRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CVoucherController {
    @Autowired
    IVoucherRepository pIVoucherRepository;
    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers() {
        try {
            List<CVoucher> pCVouchers = new ArrayList<CVoucher>();
            pIVoucherRepository.findAll().forEach(pCVouchers::add);
            return new ResponseEntity<List<CVoucher>>(pCVouchers, HttpStatus.OK);
        } catch (Exception e) {
           return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
